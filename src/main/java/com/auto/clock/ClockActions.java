package com.auto.clock;

import java.io.File;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.auto.clock.IClockActions.Checkin;
import com.auto.clock.IClockActions.ClocKType;
import com.auto.clock.IClockActions.Close;
import com.auto.clock.IClockActions.Init;
import com.auto.clock.IClockActions.Login;
import com.auto.clock.IClockActions.Logout;

public class ClockActions {
	
	private static final Logger logger = LogManager.getLogger(ClockActions.class);
	
	public static class CheckinFlow implements Init, Login, Checkin, Logout, Close {
		private WebDriver driver;

		CheckinFlow() {
		}

		@Override
		public Login init(String driverPath) throws ClockException {
			
			if (driverPath == null || "".equals(driverPath)) {
				throw new ClockException("empty driverPath: " + driverPath);
			}
			
			ChromeDriverService service = new ChromeDriverService.Builder()
					.usingDriverExecutable(new File(driverPath))
					.usingAnyFreePort()
					.build();

			ChromeOptions options = new ChromeOptions();
			//options.setHeadless(true);
			options.addArguments("--remote-allow-origins=*");
			driver = new ChromeDriver(service, options);
			return this;
		}

		@Override
		public Checkin login(String url, String username, String password) throws ClockException {

			if (url == null || "".equals(url) || 
					username == null || "".equals(username) || 
					password == null || "".equals(password)) {
				throw new ClockException("empty url/username/password: {} / {} / {}", url, username, password);
			}

			driver.get(url);
			
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(Duration.ofSeconds(60))
					.pollingEvery(Duration.ofSeconds(1))
					.ignoring(NoSuchElementException.class);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("UID")));
		
		//	顯示密碼
		//	driver.findElement(By.xpath("//*[@id=\"content\"]/div/table/tbody/tr[2]/td[2]/span/input")).click();
			driver.findElement(By.id("UID")).sendKeys(username);
			driver.findElement(By.id("KEY")).sendKeys(password);
			driver.findElement(By.id("btnLogin")).click();
			return this;
		}

		@Override
		public Logout checkin(ClocKType type) throws ClockException {
			
			driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
			
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(Duration.ofSeconds(120))
					.pollingEvery(Duration.ofSeconds(1))
					.ignoring(NoSuchElementException.class);
			
			WebElement iframe_element = wait
					.until(ExpectedConditions.visibilityOfElementLocated(By.id("mainFrame")));
			driver.switchTo().frame(iframe_element);
			
			switch (type) {
			case CHECKIN:
				driver.findElement(By.id("checkinBtn")).click();
				break;
			case CHECKOUT:
				driver.findElement(By.id("checkinBtn")).click();
				break;
			default:
				throw new ClockException("unknown ClocKType: {}", type);
			}
			
			logger.info("Clock Type {}", type);
			
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			return this;
		}

		@Override
		public Close logout() {
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath("//*[@id=\"header\"]/div[1]/ul/li[7]/a")).click();			
			logger.info("logout");
			return this;
		}

		@Override
		public void close() {
			driver.close();
			driver.quit();
			logger.info("close");
		}
	}
}
