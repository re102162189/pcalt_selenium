package com.auto.clock;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.auto.clock.IClockActions.ClocKType;
import com.auto.clock.IClockActions.OnleaveCode;

public class Main {

	private static final Logger logger = LogManager.getLogger(Main.class);

	public static void main(String[] args) {		
		ZonedDateTime zonedDateNow = LocalDateTime.now().atZone(ZoneId.of("UTC+8"));
		DayOfWeek weekday = zonedDateNow.getDayOfWeek();
		logger.info("date now: {} - {}", weekday, zonedDateNow);

		try {

			ClockParameters clockParameters = new ClockProperties().getParameters();
			
			// 補班日
			boolean isMakeup = ClockDateUtils.isMakeup(clockParameters.getMakeups(), zonedDateNow);

			if ((weekday == DayOfWeek.SATURDAY || weekday == DayOfWeek.SATURDAY) && !isMakeup) {
				logger.info("today is weekend");
				return;
			}

			if (ClockDateUtils.isHoliday(clockParameters.getHolidays(), zonedDateNow)) {
				logger.info("today is holiday");
				return;
			}

			String onleaves = clockParameters.getOnleaves();
			OnleaveCode onleaveStatus = ClockDateUtils.onleaveStatus(onleaves, zonedDateNow);
			logger.info("on leave status: {}", onleaveStatus);
			if (onleaveStatus == OnleaveCode.FULL) {
				logger.info("today is onleave");
				return;
			}

			ClocKType clockType = ClocKType.CHECKOUT;
			if (ClockTimeUtils.isValidCheckinTime(onleaveStatus, zonedDateNow.toLocalTime())) {
				clockType = ClocKType.CHECKIN;
			} else if (ClockTimeUtils.isValidCheckoutTime(onleaveStatus, zonedDateNow.toLocalTime())) {
				clockType = ClocKType.CHECKOUT;
			} else {
				logger.info("it's not a valid clock time");
				return;
			}

			int delayMins = new Random().ints(0, clockParameters.getDelay()).findFirst().getAsInt();
			logger.info("Will trigger after {} minutes", delayMins);
			Thread.sleep(delayMins * 60 * 1000);

			new ClockActions.CheckinFlow().init(clockParameters.getDriverPath())
					.login(clockParameters.getUrl(), clockParameters.getUsername(), clockParameters.getPassword())
					.checkin(ClocKType.CHECKOUT).logout().close();
			logger.info("{} Done", clockType);
		} catch (Exception e) {
			logger.error(e);
		}
	}
}