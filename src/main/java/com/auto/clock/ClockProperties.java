package com.auto.clock;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ClockProperties {
	
	private static final Logger logger = LogManager.getLogger(ClockProperties.class);
	
	private final List<String> paramKeys = new ArrayList<>();

	private final String DRIVER_PATH = "driver.path";
	private final String CLOCK_URL   = "clock.url";
	private final String CLOCK_USR   = "clock.username";
	private final String CLOCK_PWD   = "clock.password";
	private final String HOLIDAYS    = "clock.holidays";
	private final String MAKEUPS     = "clock.makeups";
	private final String ONLEAVES    = "clock.onleaves";
	private final String CLOCK_DELAY = "clock.delay";
	
	private final int DELAY_MIN = 1;
	private final int DELAY_MAX = 5;

	public ClockProperties() {
		paramKeys.add(DRIVER_PATH);
		paramKeys.add(CLOCK_URL);
		paramKeys.add(CLOCK_USR);
		paramKeys.add(CLOCK_PWD);
		paramKeys.add(HOLIDAYS);
		paramKeys.add(MAKEUPS);
		paramKeys.add(ONLEAVES);
		paramKeys.add(CLOCK_DELAY);
	}

	public ClockParameters getParameters() throws ClockException, IOException {

		Properties prop = new Properties();
		String userDirectory = System.getProperty("user.dir");
		try (InputStream input = new FileInputStream(userDirectory + "/app.properties")) {
			prop.load(input);
		} catch (IOException e) {
			logger.warn("file {}/app.properties not found. use src/main/resources/app.properties", userDirectory);
			prop.load(ClockProperties.class.getClassLoader().getResourceAsStream("app.properties"));
		}
		
		ClockParameters clockParameters = new ClockParameters();
		for (String paramKey : paramKeys) {
			String paramVal = prop.getProperty(paramKey);
			
			// onleaves / holidays / makeups/ delay_mins can be empty
			if ((paramVal == null || "".equals(paramVal)) && 
					(!ONLEAVES.equals(paramKey) && 
					 !HOLIDAYS.equals(paramKey) &&
					 !MAKEUPS.equals(paramKey) &&
					 !CLOCK_DELAY.equals(paramKey) )) {
				throw new ClockException("{} must not empty", paramKey);
			}

			switch (paramKey) {
			case DRIVER_PATH:
				clockParameters.setDriverPath(paramVal);
				break;
			case CLOCK_URL:
				clockParameters.setUrl(paramVal);
				break;
			case CLOCK_USR:
				clockParameters.setUsername(paramVal);
				break;
			case CLOCK_PWD:
				clockParameters.setPassword(paramVal);
				break;
			case HOLIDAYS:
				clockParameters.setHolidays(paramVal);
				break;
			case MAKEUPS:
				clockParameters.setMakeups(paramVal);
				break;
			case ONLEAVES:
				clockParameters.setOnleaves(paramVal);
				break;
			case CLOCK_DELAY:
				if(paramVal == null || "".equals(paramVal)) {
					paramVal = "0";
				}
				int delayMins = Integer.parseInt(paramVal);
				if (delayMins < DELAY_MIN || delayMins > DELAY_MAX) {
					throw new ClockException("delay.mins range (1-5): {}", delayMins);
				}
				clockParameters.setDelay(delayMins);
				break;
			default:
				throw new ClockException("Unknown paramKey: {}", paramKey);
			}
		}

		return clockParameters;
	}
}
