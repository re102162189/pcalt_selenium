package com.auto.clock;

public class ClockParameters {

	private String driverPath;
	
	private String url;
	
	private String username;
	
	private String password;

	private String holidays;
	
	private String makeups;
	
	private String onleaves;

	private int delay;

	public String getDriverPath() {
		return driverPath;
	}

	public void setDriverPath(String driverPath) {
		this.driverPath = driverPath;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHolidays() {
		return holidays;
	}

	public void setHolidays(String holidays) {
		this.holidays = holidays;
	}

	public String getMakeups() {
		return makeups;
	}

	public void setMakeups(String makeups) {
		this.makeups = makeups;
	}

	public String getOnleaves() {
		return onleaves;
	}

	public void setOnleaves(String onleaves) {
		this.onleaves = onleaves;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}
}
