package com.auto.clock;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.auto.clock.IClockActions.OnleaveCode;

public class ClockDateUtils {
	
	private static final String SPLIT_SYMBOL = ",";
	
	// yyyy-MM-dd#M, yyyy-MM-dd#A, yyyy-MM-dd#F
	private static final String ONLEAVE_REGEX = "^\\d{4}-\\d{2}-\\d{2}#(F|M|A)$";
	
	public static boolean isMakeup(String days, ZonedDateTime zonedDateNow) throws ClockException {
		return isHoliday(days, zonedDateNow);
	}
	
	public static boolean isHoliday(String days, ZonedDateTime zonedDateNow) throws ClockException {
		
		if(days == null || "".equals(days)) {
			return false;
		}
		
		List<String> dayList = Arrays.asList(days.split(SPLIT_SYMBOL));
		for (String date : dayList) {
			if (!isDateValid(date)) {
				throw new ClockException("wrong date format: {}", date);
			}
		}

		String dateNow = getDateNow(zonedDateNow);
		return dayList.contains(dateNow);
	}

	public static OnleaveCode onleaveStatus(String onleaves, ZonedDateTime zonedDateNow) throws ClockException {
		
		if(onleaves == null || "".equals(onleaves)) {
			return OnleaveCode.NONE;
		}
		
		List<String> onleaveList = Arrays.asList(onleaves.split(SPLIT_SYMBOL));
		Map<String, OnleaveCode> onleaveMap = new HashMap<String, OnleaveCode>();		
		Pattern pattern = Pattern.compile(ONLEAVE_REGEX);

		for (String date : onleaveList) {
			if (!pattern.matcher(date).matches()) {
				throw new ClockException("wrong format (yyyy-MM-dd#M, yyyy-MM-dd#A, yyyy-MM-dd#F): " + date);
			}

			String[] splited = date.split("#");
			String leaveDate = splited[0];
			
			if (!isDateValid(leaveDate)) {
				throw new ClockException("wrong date format (yyyy-MM-dd): " + leaveDate);
			}
			
			if (onleaveMap.containsKey(leaveDate)) {
				throw new ClockException("duplicate onleave date: " + leaveDate);
			}
			
			String onleaveCode = splited[1];

			switch (onleaveCode) {
			case "M":
				onleaveMap.put(leaveDate, OnleaveCode.MORNING);
				break;
			case "A":
				onleaveMap.put(leaveDate, OnleaveCode.AFTERNOON);
				break;
			case "F":
				onleaveMap.put(leaveDate, OnleaveCode.FULL);
				break;
			default:
				throw new ClockException("unknown onleave code {}, please use [M, A, F]", onleaveCode);
			}
		}

		String dateNow = getDateNow(zonedDateNow);
		if (onleaveMap.containsKey(dateNow)) {
			return onleaveMap.get(dateNow);
		} else {
			return OnleaveCode.NONE;
		}
	}
	
	private static String getDateNow(ZonedDateTime zonedDateNow) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String dateNow = zonedDateNow.format(formatter);
		return dateNow;
	}

	private static boolean isDateValid(final String date) {
		boolean valid = true;
		try {
			LocalDate.parse(date, DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle(ResolverStyle.STRICT));
		} catch (DateTimeParseException e) {
			valid = false;
		}
		return valid;
	}
}