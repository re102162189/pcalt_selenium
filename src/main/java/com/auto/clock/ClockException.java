package com.auto.clock;

import java.util.Formatter;

public class ClockException extends Exception {
	
	private static final Formatter FORMATTER = new Formatter();
	private static final long serialVersionUID = 1L;

	public ClockException(String format, Object... args) {
		super(FORMATTER.format(format, args).toString());
    }
}
