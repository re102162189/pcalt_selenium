package com.auto.clock;

import java.time.LocalTime;

import com.auto.clock.IClockActions.OnleaveCode;

public class ClockTimeUtils {

	private static final LocalTime morningStart = LocalTime.of(8, 59, 59);
	private static final LocalTime morningEnd = LocalTime.of(9, 31, 00);

	private static final LocalTime noonStart = LocalTime.of(12, 59, 59);
	private static final LocalTime noonEnd = LocalTime.of(13, 31, 00);

	private static final LocalTime afternoonStart = LocalTime.of(18, 29, 59);
	private static final LocalTime afternoonEnd = LocalTime.of(19, 01, 00);

	public static boolean isValidCheckinTime(OnleaveCode onleaveStatus, LocalTime timeNow) throws Exception {

		boolean isValid = false;

		switch (onleaveStatus) {
		case NONE:
		case AFTERNOON:
			isValid = timeNow.isAfter(morningStart) && timeNow.isBefore(morningEnd);
			break;
		case MORNING:
			isValid = timeNow.isAfter(noonStart) && timeNow.isBefore(noonEnd);
			break;
		default:
			break;
		}

		return isValid;
	}

	public static boolean isValidCheckoutTime(OnleaveCode onleaveStatus, LocalTime timeNow) throws Exception {

		boolean isValid = false;

		switch (onleaveStatus) {
		case AFTERNOON:
			isValid = timeNow.isAfter(noonStart) && (timeNow.isBefore(noonEnd));
			break;
		case NONE:
		case MORNING:
			isValid = timeNow.isAfter(afternoonStart) && timeNow.isBefore(afternoonEnd);
			break;
		default:
			break;
		}

		return isValid;
	}
}
