package com.auto.clock;

public interface IClockActions {

	enum ClocKType {
		CHECKIN, CHECKOUT
	}

	enum OnleaveCode {
		FULL, MORNING, AFTERNOON, NONE
	}

	interface Init {
		Login init(String driverPath) throws ClockException;
	}

	interface Login {
		Checkin login(String url, String username, String password) throws ClockException;
	}

	interface Checkin {
		Logout checkin(ClocKType type) throws ClockException;
	}

	interface Logout {
		Close logout();
	}

	interface Close {
		void close();
	}
}
