##### preconditions
```
1. jdk 17+
2. maven 3.9+
```

##### download [chromedriver](https://chromedriver.chromium.org/downloads) based on your OS and Chrome version

##### setup parameters in app.properties, [holidays](https://www.dgpa.gov.tw/information?uid=83&pid=10654)
```
driver.url=/*path_to_driver*/chromedriver
clock.username={your_id}
clock.password={your_passwd}

# yyyy-MM-dd 
clock.holidays=2022-01-31,2022-02-01,2022-02-02,2022-02-03,2022-02-04,2022-02-05,2022-02-28,2022-04-04,2022-04-05,2022-06-03,2022-09-09,2022-10-10

# yyyy-MM-dd#{leave_code} (M: morning, A: afternoon, F: full)
clock.onleaves=

# yyyy-MM-dd 
clock.makeups=2022-01-22

# unit: mins (min: 0, max: 5)
clock.delay=5
```

##### maven build
```
$ mvn clean package
$ cp target/clock-0.0.1-SNAPSHOT-jar-with-dependencies.jar /*path_to_jar*/clock.jar
$ cp src/main/resources/app.properties /*path_to_jar*/app.properties
```

##### setup ctrontab. i.e. MacOS
```
$ crontab -e

# ====================checkin every weekday 09:00=====================
0 9 * * 1-5 java -jar   /*path_to_jar*/clock.jar >> /*path_to_log*/clock.log

# ====================checkin every weekday 13:00=====================
00 13 * * 1-5 java -jar /*path_to_jar*/clock.jar >> /*path_to_log*/clock.log

# ====================checkout every weekday 18:30====================
30 18 * * 1-5 java -jar /*path_to_jar*/clock.jar >> /*path_to_log*/clock.log

# check crontab setup
$ crontab -l
```